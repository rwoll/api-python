#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name="twinwaveapi",
    version="0.6.0",
    description="Twinwave Security API",
    author="Twinwave Security",
    author_email="bryan@twinwave.io",
    url="https://gitlab.com/twinwave-public/api-python",
    packages=find_packages(),
    entry_points={"console_scripts": ["twcli=twinwaveapi.twcli:main"]},
    install_requires=["requests"],
)
