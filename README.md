# api-python

python library for interacting with the Twinwave API

## Installation

Install by running `pip3 install .`

## CLI Usage

To use, set the TWMP_APIKEY environment variable to a valid API key, then run twcli:

```bash
$ export TWMP_APIKEY=...
$ twcli -h
usage: twcli [-h] [-j JOBID] [-cf JOBID] [-u URL] [-f FILE] [-lj] [-le] [-e ENGINE] [-p PRIORITY]

optional arguments:
  -h, --help            show this help message and exit
  -j JOBID, --getjob JOBID
                        JobID to query
  -cf JOBID, --consolidated JOBID
                        get consolidated forensics for a jobid
  -u URL, --submiturl URL
                        URL to submit
  -f FILE, --submitfile FILE
                        file to submit
  -lj, --listjobs       list recent jobs
  -le, --listengines    list available engines
  -e ENGINE, --engine ENGINE
                        add the engine to the list of desired engines
  -p PRIORITY, --priority PRIORITY
                        specify a priority (1-255) for the submission. Lower priority values are processed first
```
