#!/usr/bin/env python3
import sys
import os
import argparse
import json
from . import TwinwaveMAP, PRIORITY_NORMAL


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-j", "--getjob", type=str, metavar="JOBID", help="JobID to query")
    parser.add_argument("-cf", "--consolidated", type=str, metavar="JOBID", help="get consolidated forensics for a jobid")
    parser.add_argument("-u", "--submiturl", type=str, metavar="URL", help="URL to submit")
    parser.add_argument("-f", "--submitfile", type=argparse.FileType("rb"), metavar="FILE", help="file to submit")
    parser.add_argument("-lj", "--listjobs", action="store_true", help="list recent jobs")
    parser.add_argument("-le", "--listengines", action="store_true", help="list available engines")
    parser.add_argument("-e", "--engine", action="append", metavar="ENGINE", help="add the engine to the list of desired engines")
    parser.add_argument(
        "-p",
        "--priority",
        type=int,
        metavar="PRIORITY",
        help="specify a priority (1-255) for the submission. Lower priority values are processed first",
        default=PRIORITY_NORMAL,
    )
    parser.add_argument("--profile", type=str, metavar="PROFILE", help="specify an optional submission profile", default="default")

    args = parser.parse_args()

    api = None
    try:
        apikey = os.environ["TWMP_APIKEY"]
        host = os.environ.get("TWMP_APIHOST")

        if host:
            api = TwinwaveMAP(apikey, host)
        else:
            api = TwinwaveMAP(apikey)
    except KeyError:
        print("Missing configuration environment variable. Must set TWMP_APIKEY")
        sys.exit(1)

    resp = None
    if args.listjobs:
        resp = api.get_recent_jobs()
    elif args.consolidated:
        resp = api.get_consolidated_forensics(args.consolidated)
    elif args.submiturl:
        resp = api.submit_url(args.submiturl, args.engine, args.priority, args.profile)
    elif args.submitfile:
        resp = api.submit_file(args.submitfile, args.engine, args.priority, args.profile)
    elif args.getjob:
        resp = api.get_job(args.getjob)
    elif args.listengines:
        resp = api.get_engines()

    print(json.dumps(resp))


if __name__ == "__main__":
    main()
