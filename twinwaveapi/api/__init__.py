#!/usr/bin/env python3
import os
import sys
import requests
import json
import argparse
import termcolor

API_HOST = "https://api.twinwave.io"

# example priority values
PRIORITY_HIGHEST = 1
PRIORITY_HIGH = 5
PRIORITY_NORMAL = 10
PRIORITY_LOW = 15
PRIORITY_LOWER = 20


class AuthenticationException(Exception):
    pass


class TwinwaveMAP:
    def __init__(self, apikey, host=API_HOST):
        self.host = host
        self.apikey = apikey

    def _get_header(self):
        return {"X-API-KEY": self.apikey}

    def get_recent_jobs(self, numjobs=10):
        url = f"{self.host}/v1/jobs/recent"

        resp = requests.get(url, params={"count": numjobs}, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def get_engines(self):
        url = f"{self.host}/v1/engines"

        resp = requests.get(url, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def get_job(self, jobid):
        url = f"{self.host}/v1/jobs/{jobid}"

        resp = requests.get(url, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def get_forensics(self, jobid, taskid):
        url = f"{self.host}/v1/jobs/{jobid}/tasks/{taskid}/forensics"

        resp = requests.get(url, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def get_consolidated_forensics(self, jobid):
        url = f"{self.host}/v1/jobs/{jobid}/forensics"

        resp = requests.get(url, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def submit_url(self, scanurl, engine_list=[], priority=PRIORITY_NORMAL, profile="default"):
        url = f"{self.host}/v1/jobs/urls"

        req = {"url": scanurl, "engines": engine_list, "priority": priority, "profile": profile}

        resp = requests.post(url, json=req, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def submit_file(self, fileobj, engine_list=[], priority=PRIORITY_NORMAL, profile="default"):
        url = f"{self.host}/v1/jobs/files"

        resp = requests.post(
            url,
            files={
                "engines": (None, json.dumps(engine_list)),
                "filedata": fileobj,
                "filename": (None, os.path.basename(fileobj.name)),
                "priority": (None, priority),
                "profile": (None, profile),
            },
            headers=self._get_header(),
        )

        resp.raise_for_status()

        return resp.json()
